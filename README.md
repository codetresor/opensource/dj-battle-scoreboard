## DJ Battle Scoreboard - DJ Roulette ##


---

### What is it? ###

This is a javaFX "dj battle score board" for events with several DJ's, fighting for votes and leads to play their tunes. Originally, this application was built for our friends and family from the [Kurhaus Lenzerheide in Switzerland, i.e. their in-house club called "Cinema Club"](http://www.kurhaus-lenzerheide.ch/)!

A pretty much a straight forward project to run a native style app on a OS X based device, to visualize a dj battle score board in a club.

The bars from the chart change colors with every reset of the votes. to increase a vote for a certain dj, just click on its bar and the votes increase by one (+1). The DJ with most votes is shown up as the leader.

---

### Customize the Data/DJ's/Images ###

Loading a CSV data file: press `<shift> + <ctrl> + <Enter>` to open a file load dialog. An example of the csv data file you can find in the resource package (src/main/resources/data/djs.csv). This file is also the default, so if you like to change the default behaviour before you build, use this CSV file in the resources.

To load the DJ Data on the fly (as described before, use the shortcut `<shift> + <ctrl> + <Enter>`), use a CSV file, which consists out of comma separated Headers ( id | name | soundStyle | votes | avatarPicPath32 | avatarPicPathMain ) and its data formats, following described:

* *id* - the number of the entry. this also represents the order.
* *name* - the DJ name.
* *soundStyle* - the sound/music style the DJ represents.
* *votes* - the start votes number. should be set at least to 10, so the animation looks better.
* *avatarPicPath32* - the small DJ profile image as a file path (relative to the CSV file root), which must have a resolution of 32 x 32 Pixels / 72 ppi.
* *avatarPicMain* - the DJ profile image as a file path (relative to the CSV file root). the image must have a resolution of 250 x 250 Pixels / 72 ppi.

Please find an example of the default CSV file [here](http://codetresor.com/opensource/dj-battle-scoreboard/blob/master/src/main/resources/data/djs.csv). 

---

### Build the App ###

To build the application as a OS X Application, use the following maven commands:

+ `mvn clean package` builds the application jar

+ `mvn clean jfx:generate-key-store` Generates a development keysstore that can be used for signing web based distribution bundles based on POM settings. You only need to run this command once and then you can include the resulting keystore in your source control. There is no harm in re-running the command however, it will simply overwrite the keystore with a new one. The resulting keystore is useful for simplifying development but should not be used in a production environment. You should get a legitimate certificate from a certifier and include that keystore in your codebase. Using this testing keystore will result in your users seeing the ugly warning about untrusted code.

+ `mvn clean jfx:native` generates a native deployment bundle (f.e. MSI, EXE, DMG, RPG, etc)

---

### Future ToDo's ###

Style the app with some fancy javaFX animation shizzle! ;)

---

That's it, that's all for the moment! 


Best regards,

*maesi - tuason software inc.*
